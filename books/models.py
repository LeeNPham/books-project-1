
from django.db import models
from django.contrib.auth.models import User




class Author(models.Model):
    author_name = models.CharField(max_length=200, unique=True, null=True, blank=True)

    def __str__(self):
        return self.author_name


class Book(models.Model):
    title = models.CharField(max_length=200, unique=True, blank=True)
    author = models.ManyToManyField(
        Author, related_name="books", max_length=200, blank=True)
    pages = models.SmallIntegerField(null=True, blank=True)
    isbn = models.BigIntegerField(null=True, blank=True)
    year_published = models.SmallIntegerField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    image = models.URLField(null=True, blank=True)
    in_print = models.BooleanField(null=True, blank=True)

    def __str__(self):
        return self.title + " by " + str(self.author.first())


class BookReview(models.Model):
    user = models.ForeignKey(User, related_name="reviewers", on_delete=models.CASCADE)
    book = models.ForeignKey(
        Book, related_name="reviews", on_delete=models.CASCADE)
    text = models.TextField(null=True, blank=True)

    def __str__(self):
        return str(self.user)


class Magazine(models.Model):
    creator = models.ForeignKey(User, related_name="magazines", on_delete=models.CASCADE )
    title = models.CharField(max_length=200, unique=True, blank=True)
    release_cycle = models.CharField(
        max_length=200, blank=True)  # yearly, quarterly or weekly
    description = models.TextField(null=True, blank=True)  # Genre description
    image = models.URLField(null=True, blank=True)

    def __str__(self):
        return self.title


class Issue(models.Model):
    magazine_title = models.ForeignKey(
        Magazine, related_name="issues", on_delete=models.CASCADE, null=True)

    issue_title = models.CharField(max_length=200, blank=True, null=True)
    date_published = models.SmallIntegerField(null=True, blank=True)
    page_count = models.SmallIntegerField(null=True, blank=True)
    issue_number = models.SmallIntegerField(null=True, blank=True)
    cover_issue_image = models.URLField(null=True, blank=True)
    description = description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.issue_title



class Genre(models.Model):
    name = models.CharField(max_length=200, unique=True, blank=True, null=True)
    magazines = models.ManyToManyField(Magazine, related_name="genres", blank=True)
    books = models.ManyToManyField(Book, related_name="genres", blank=True)

    def __str__(self):
        return self.name 