from django.contrib import admin
from .models import Author, Book, BookReview, Genre, Issue, Magazine

# class BookAdmin(Book):
#     pass
#replace Book with BookAdmin if you use the two lines of code from above

admin.site.register(Book)
admin.site.register(Magazine)
admin.site.register(BookReview)
admin.site.register(Author)
admin.site.register(Issue)
admin.site.register(Genre)
# Register your models here.