from dataclasses import fields
from django import forms
from .models import Author, Book
from .models import Magazine, Issue

# creating a form


class BookForm(forms.ModelForm):
    class Meta:
        model = Book
        exclude = ["author"]


class AuthorForm(forms.ModelForm):
    class Meta:
            model = Author
            fields =[
                "author_name"
            ]





class MagazineForm(forms.ModelForm):
    class Meta:
        model = Magazine
        fields = [
            "title",
            "release_cycle",
            "description",
            "image",
        ]


class IssueForm(forms.ModelForm):
    class Meta:
        model = Issue
        fields = [
            "magazine_title",
            "issue_title",
            "date_published",
            "page_count",
            "issue_number",
            "cover_issue_image",
            "description"
        ]
