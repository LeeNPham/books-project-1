from django.urls import path
from .views import books_detail, books_list, contents_list, create_new, create_new_magazine, delete_book, delete_book, delete_magazine, magazines_detail, magazines_list, update_book, update_magazine, show_genres, list_my_reviews


urlpatterns = [
    
    path("", contents_list, name="home"),
    path("books", books_list, name="books_list"),
    path("<int:pk>/detail", books_detail, name="book_detail"),
    path("new", create_new, name="create_new"),
    path("<int:pk>/update", update_book, name="update_book"),
    path("<int:pk>/delete", delete_book, name="delete_book"),

    path("magazine", magazines_list, name="magazines_list"),
    path("<int:pk>/detail_magazine", magazines_detail, name="magazine_detail"),
    path("new_magazine", create_new_magazine, name="create_new_magazine"),
    path("<int:pk>/update_magazine", update_magazine, name="update_magazine"),
    path("<int:pk>/delete_magazine", delete_magazine, name="delete_magazine"),

    path("genre/",show_genres, name="show_genres"),
    path('reviews/', list_my_reviews, name="list_reviews"),
]