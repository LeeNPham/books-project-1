from django.shortcuts import redirect, render
from .models import Book, Genre, Magazine
from .forms import BookForm, MagazineForm, AuthorForm
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required



@login_required
def list_my_reviews(request):
    reviews = request.user.reviewers.all()
    # user=request.user
    context = {
        # "user": user,
        "reviews": reviews

    }
    return render(request, "books/review_list.html", context)


def contents_list(request):
    list_of_books = Book.objects.all()
    list_of_magazines = Magazine.objects.all()
    context = {
        "books": list_of_books,
        "magazines": list_of_magazines,
        "heading": "My List Of Content",
    }
    return render(request, "books/home.html", context)


def books_list(request):
    list_of_books = Book.objects.all()
    context = {
        "books": list_of_books,
        "heading": "My List Of Books",
    }
    return render(request, "books/book_list.html", context)


def books_detail(request, pk):
    context = {
        "book": Book.objects.get(pk=pk) if Book else [],
        "heading": "Details on Book",
    }
    return render(request, "books/detail.html", context)

##########################################################


def create_new(request):
    context = {
        "head": "Adding A New Book"
    }

    form = BookForm(request.POST or None)
    if form.is_valid():
        book = form.save()
        return redirect("book_detail", pk=book.pk)
    context['form'] = form

    form2 = AuthorForm(request.POST or None)
    if form2.is_valid():
        form2.save()
        return redirect("create_new")
    context['form2'] = form2

    return render(request, "books/new.html", context)
###########################################################


def update_book(request, pk):
    context = {}
    obj = get_object_or_404(Book, pk=pk)
    form = BookForm(request.POST or None, instance=obj)
    if form.is_valid():
        book = form.save()
        return redirect("book_detail", pk=book.pk)
    context['form'] = form
    return render(request, "books/update.html", context)


def delete_book(request, pk):
    context = {}
    obj = get_object_or_404(Book, pk=pk)
    if request.method == "POST":
        obj.delete()
        return redirect("home")
    return render(request, "books/deleted.html", context)


def magazines_list(request):
    list_of_magazines = Magazine.objects.all()
    context = {
        "magazines": list_of_magazines,
        "heading": "My List Of magazines",
    }
    return render(request, "magazines/magazine_list.html", context)


def magazines_detail(request, pk):
    context = {
        "magazine": Magazine.objects.get(pk=pk) if Magazine else [],
        "heading": "Details on Magazine",
    }
    return render(request, "magazines/detail.html", context)


def create_new_magazine(request):
    context = {
        "head": "Adding A New Magazine"
    }
    form = MagazineForm(request.POST or None)
    if form.is_valid():
        magazine = form.save()
        return redirect("magazine_detail", pk=magazine.pk)
    context['form'] = form
    return render(request, "magazines/new.html", context)


def update_magazine(request, pk):
    context = {}
    obj = get_object_or_404(Magazine, pk=pk)
    form = MagazineForm(request.POST or None, instance=obj)
    if form.is_valid():
        magazine = form.save()
        return redirect("magazine_detail", pk=magazine.pk)
    context['form'] = form
    return render(request, "magazines/update.html", context)


def delete_magazine(request, pk):
    context = {}
    obj = get_object_or_404(Magazine, pk=pk)
    if request.method == "POST":
        obj.delete()
        return redirect("home")
    return render(request, "magazines/deleted.html", context)


def show_genres(request):
    genres = Genre.objects.all()

    context = {
        "genres": genres,
        "heading": "Genres"
    }
    return render(request, "books/genre_view.html", context)
